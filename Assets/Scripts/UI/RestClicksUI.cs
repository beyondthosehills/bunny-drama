﻿using UnityEngine;
using BunnyDrama.Framework;
using Prime31.MessageKit;
using UnityEngine.UI;

namespace BunnyDrama.UI
{

    public class RestClicksUI : MonoBehaviour 
    {
        Text text;

        void Awake ()
        {
            text = this.GetComponent<Text> ();
            MessageKit<int>.addObserver (GameMessage.CountClick, UpdateCount);
        }

        void OnDestroy ()
        {
            MessageKit<int>.removeObserver (GameMessage.CountClick, UpdateCount);
        }

        void UpdateCount (int restClickCount)
        {
            text.text = restClickCount.ToString ();
        }
    	
    }
}
