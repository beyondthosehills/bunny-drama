﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using BunnyDrama.Framework;

public class CarrotBehaviour : MonoBehaviour 
{

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.tag == "Player")
        {
            MessageKit.post (GameMessage.GetCarrot);
            this.gameObject.SetActive (false);

        }
    }
}
