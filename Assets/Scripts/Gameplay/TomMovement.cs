﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using BunnyDrama.Framework;

namespace BunnyDrama.Gameplay
{
    public class TomMovement : MonoBehaviour 
    {
        [SerializeField] Transform startLocation;
        [SerializeField] Transform endLocation;

    	// Use this for initialization
    	void Awake ()
        {
            this.transform.position = startLocation.position;
            MessageKit.addObserver (GameMessage.CountClick, UpdateMovement);
    	}

        void OnDestroy ()
        {
            MessageKit.removeObserver (GameMessage.CountClick, UpdateMovement);
        }
    	
        void UpdateMovement()
        {
            float value = (float)LevelManager.CurrentClickCount / (float)LevelManager.MaxClickCount;

            this.transform.position = Vector3.Lerp (startLocation.position, endLocation.position, value);
        }
    }
}
