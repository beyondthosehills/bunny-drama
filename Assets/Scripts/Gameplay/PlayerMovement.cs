﻿using UnityEngine;
using Prime31.MessageKit;
using BunnyDrama.Framework;
using System.Collections;

namespace BunnyDrama.Gameplay
{
    public class PlayerMovement : MonoBehaviour 
    {
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] float velocity = 1f;
        [SerializeField] LayerMask tileLayerMask;
        [SerializeField] LayerMask blockLayerMask;
        [SerializeField] Animator animator;

        private bool isEnabled = false;

        void Awake ()
        {
            MessageKit.addObserver (GameMessage.BeginLevel, Enable);
            MessageKit.addObserver (GameMessage.EndLevel, Disable);
            MessageKit.addObserver (GameMessage.GameOver, Disable);
            MessageKit.addObserver (GameMessage.GetCarrot, SetCarrotAnimationTrigger);
        }

        void OnDestroy ()
        {
            MessageKit.removeObserver (GameMessage.BeginLevel, Enable);
            MessageKit.removeObserver (GameMessage.EndLevel, Disable);
            MessageKit.removeObserver (GameMessage.GameOver, Disable);
            MessageKit.removeObserver (GameMessage.GetCarrot, SetCarrotAnimationTrigger);

        }

        void SetCarrotAnimationTrigger ()
        {
            animator.SetTrigger ("HasCarrot");
        }

        void Enable ()
        {
            isEnabled = true;
        }

        void Disable ()
        {
            isEnabled = false;
        }

    	// Update is called once per frame
    	void Update () 
        {
            if (!isEnabled)
                return;

            if (Input.GetKeyUp (KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            {
                Move (Vector2.right);
                return;
            }

            if (Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
            {
                Move (Vector2.left);
                return;
            }

            if (Input.GetKeyUp (KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow))
            {
                Move (Vector2.up);
                return;
            }

            if (Input.GetKeyUp (KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                Move (Vector2.down);
                return;
            }
    	}

        void Move (Vector3 moveDirection)
        {
            if (!Raycast (moveDirection))
            {
                MessageKit.post (GameMessage.CountClick);
                //this.transform.position = this.transform.position + moveDirection;

                if (moveDirection.x > 0.5f)
                {
                    spriteRenderer.flipX = false;
                }

                if (moveDirection.x < 0.5f)
                {
                    spriteRenderer.flipX = true;
                }

                StartCoroutine (MovePlayer (moveDirection));

            }
        }


        IEnumerator MovePlayer (Vector3 moveDirection)
        {
            isEnabled = false;

            animator.SetBool ("Move", true);

            Vector3 initPosition = this.transform.position;

            while (Vector3.Distance(initPosition,this.transform.position) < 1f )
            {
                transform.position = transform.position + (moveDirection * velocity * Time.deltaTime);
                yield return  null;
            }

            // for safe reasons
            this.transform.position = initPosition + moveDirection;

            animator.SetBool ("Move", false);

            isEnabled = true;
        }

        bool Raycast (Vector3 moveDirection)
        {
            RaycastHit2D tileRaycastHit = Physics2D.Raycast (this.transform.position, moveDirection, 1f, tileLayerMask.value);
            RaycastHit2D blockRaycastHit = Physics2D.Raycast (this.transform.position, moveDirection, 1f, blockLayerMask.value);

            if (tileRaycastHit.collider != null)
            {
                TileBehaviour tile = tileRaycastHit.collider.GetComponent<TileBehaviour> ();
                tile.RevealTile ();

                // move
                if (blockRaycastHit.collider == null)
                {
                    return false;
                }
                // don't move and count click
                else
                {
                    MessageKit.post (GameMessage.CountClick);
                    return true;
                }
            }

            // don't move
            if (blockRaycastHit.collider != null)
            {
                return true;
            }

            // move
            return false;

        }
    }
}
