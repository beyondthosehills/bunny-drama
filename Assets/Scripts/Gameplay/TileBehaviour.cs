﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BunnyDrama.Gameplay
{
    public enum TileState
    {
        Hidden,
        Revealed
    }

    public class TileBehaviour : MonoBehaviour 
    {
        [SerializeField] GameObject frontGroundTile;
        [SerializeField] Collider2D tileCollider;


        void Awake ()
        {
            State = TileState.Hidden;
        }

        private TileState _state;
        public TileState State
        {
            get
            {
                return _state;
            }

            private set
            {
                _state = value;
                switch (_state)
                {
                case TileState.Hidden:
                    Hide ();
                    break;
                case TileState.Revealed:
                    Reveal ();
                    break;
                }
            }    
        }

        public void RevealTile ()
        {
            State = TileState.Revealed;
        }

        void Hide ()
        {
            frontGroundTile.SetActive (true);
            tileCollider.enabled = true;
        }

        void Reveal ()
        {
            frontGroundTile.SetActive (false);
            tileCollider.enabled = false;
        }

    	
    }
}
