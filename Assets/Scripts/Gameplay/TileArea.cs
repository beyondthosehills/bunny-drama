﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BunnyDrama.Gameplay
{
    public class TileArea : MonoBehaviour 
    {
        public enum ViewMode
        {
            Outline,
            Grid,
            Solid
        }

        [SerializeField] int width;
        [SerializeField] int height;
        [SerializeField] ViewMode viewMode = ViewMode.Outline;
        [SerializeField] TileBehaviour tilePrefab;

    	// Use this for initialization
    	void Awake () 
        {
            GenerateTiles ();
    	}

        void GenerateTiles ()
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Instantiate<TileBehaviour> (
                        tilePrefab, 
                        new Vector3 (
                            this.transform.position.x + x,
                            this.transform.position.y + y, 
                            0), 
                        Quaternion.identity,
                        this.transform);
                    
                }
            }
        }

        void OnDrawGizmos ()
        {
            switch (viewMode)
            {
            case ViewMode.Outline:
                DrawWireCube ();
                break;
            case ViewMode.Grid:
                DrawGrid ();
                DrawWireCube ();
                break;
            case ViewMode.Solid:
                DrawSolid ();
                DrawGrid ();
                DrawWireCube ();
                break;
            }

        }
    	
        void DrawGrid ()
        {
            Gizmos.color = Color.magenta;

            float _positionX = this.transform.position.x - 0.5f;
            float _positionY = this.transform.position.y - 0.5f;

            for (int x = 0; x < width; x++)
            {
                Gizmos.DrawLine (
                    new Vector3 (_positionX + x, _positionY, 0),
                    new Vector3 (_positionX + x, _positionY + height, 0)
                );
            }

            for (int y = 0; y < height; y++)
            {
                Gizmos.DrawLine (
                    new Vector3 (_positionX, _positionY + y, 0),
                    new Vector3 (_positionX + width, _positionY + y, 0)
                );
            }

        }

        void DrawWireCube ()
        {
            Gizmos.color = Color.red;

            Gizmos.DrawWireCube (
                new Vector3(
                    this.transform.position.x - 0.5f +((float)width * 0.5f),
                    this.transform.position.y - 0.5f +((float)height * 0.5f),
                    0f),
                new Vector3 (width, height, 1f));
        }

        void DrawSolid ()
        {
            Gizmos.color = new Color (1f, 0f, 0f, 0.5f);

            Gizmos.DrawCube (
                new Vector3(
                    this.transform.position.x - 0.5f +((float)width * 0.5f),
                    this.transform.position.y - 0.5f +((float)height * 0.5f),
                    0f),
                new Vector3 (width, height, 1f));
        }
    }
}
