﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using BunnyDrama.UI;
using UnityEngine.SceneManagement;

namespace BunnyDrama.Framework
{
    public class LevelManager : MonoBehaviour 
    {
        [SerializeField] int maxClickCount;

        int currentClickCount;

        private static LevelManager instance;

        public static int MaxClickCount
        {
            get
            {
                if (instance == null)
                    return 0;
                return instance.maxClickCount;
            }
        }

        public static int CurrentClickCount
        {
            get
            {
                if (instance == null)
                    return 0;
                return instance.currentClickCount;
            }
        }

    	// Use this for initialization
    	void Awake () 
        {
            MessageKit.addObserver (GameMessage.CountClick, OnClickCount);
            MessageKit.addObserver (GameMessage.EndLevel, EndLevel);
            MessageKit.addObserver (GameMessage.GetCarrot, OnAddClick);

            if (instance == null)
                instance = this;
    	}

        void OnDestroy ()
        {
            MessageKit.removeObserver (GameMessage.CountClick, OnClickCount);
            MessageKit.removeObserver (GameMessage.EndLevel, EndLevel);
            MessageKit.removeObserver (GameMessage.GetCarrot, OnAddClick);
        }

        void Start ()
        {
            InitLevel ();
        }

        void Update ()
        {
            if (Input.GetKeyUp (KeyCode.Escape))
            {
                SceneManager.LoadScene (0, LoadSceneMode.Single);
            }
        }

        void InitLevel ()
        {
            currentClickCount = 0;
            MessageKit<int>.post (GameMessage.CountClick, RestClickCount());

            MessagePanelUI.ShowMessage ("Start Level!", "start", ()=>MessageKit.post(GameMessage.BeginLevel));
        }

        void OnAddClick ()
        {
            currentClickCount--;
            MessageKit<int>.post (GameMessage.CountClick, RestClickCount());
        }
    	
        void OnClickCount ()
        {
            currentClickCount++;

            MessageKit<int>.post (GameMessage.CountClick, RestClickCount());

            if (RestClickCount() <= 0)
            {
                GameOver ();
            }
        }

        void EndLevel ()
        {
            MessagePanelUI.ShowMessage ("Level Completed!", "next level", GoToNextLevel);
        }

        void GoToNextLevel ()
        {
            int currentSceneBuildIndex = SceneManager.GetActiveScene ().buildIndex;
            int nextSceneBuildIndex = currentSceneBuildIndex + 1;
            Debug.Log (SceneManager.sceneCountInBuildSettings);
            nextSceneBuildIndex = nextSceneBuildIndex % SceneManager.sceneCountInBuildSettings;
            SceneManager.LoadScene (nextSceneBuildIndex, LoadSceneMode.Single);
        }

        void RestartLevel ()
        {
            SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex, LoadSceneMode.Single);
        }

        void GameOver ()
        {
            MessageKit.post (GameMessage.GameOver);
            MessagePanelUI.ShowMessage ("Level Lost!", "repeat level", RestartLevel);
        }

        int RestClickCount ()
        {
            return maxClickCount - currentClickCount;
        }
    }
}
